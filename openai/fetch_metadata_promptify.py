import sys
import os
from jsonschema import validate
from pathlib import Path
import json
import coloredlogs, logging
from promptify import Prompter, OpenAI
import dateparser
import re

# pattern = "/{(.*?)}/"

coloredlogs.install(level="DEBUG")

BASE_DIR = Path(__file__).resolve().parent.parent

with open(
    os.path.join("/code/combio_app/static/combio_app/metadata_scheme/combio_metadata_scheme.json"),
    encoding="utf-8",
) as schema_file:
    metadata_schema = json.loads(schema_file.read())

sys.path.append("/code/")
os.environ["DJANGO_SETTINGS_MODULE"] = "project.settings"

import django

django.setup()
from combio_app.models import Record, Collection

with open("../env/openai.txt") as f:
    openai_key = f.read()
    print(openai_key)

exit()

collection = Collection.objects.get(name="Wellcome Trust Seminars")

for record in collection.record_set.all()[0:1]:
    print(record.id)
    transcript = record.transcript()[0:1500]
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"create a valid JSON object from the information you will extract from the following interview transcript :\n'{transcript}'.\n The JSON object should follow this JSON schema:{metadata_schema}.\nThe format of the date field should be the following: '%Y-%m-%d'. this should be the date when the interview took place.",
        max_tokens=1000,
    )
    print("====RESPONSE====")
    print(response.choices[0].text.strip())
    # clean_metadata = re.match(pattern, response.choices[0].text.strip())
    json_metadata = json.loads(response.choices[0].text.strip())
    json_metadata["combio"]["title"] = record.metadata["combio"]["title"]
    json_metadata["combio"]["collection"] = record.metadata["combio"]["collection"]
    json_metadata["dc"]["description"] = record.metadata["dc"]["description"]
    json_metadata["combio"]["permalink"] = record.metadata["combio"]["permalink"]
    json_metadata["combio"]["transcript"] = record.transcript()
    validate(instance=json_metadata, schema=metadata_schema)
    # print(json_metadata)
    record.metadata = json_metadata
    record.save()
    print(json_metadata)
    print(record.id)
