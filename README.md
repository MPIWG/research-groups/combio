# Commoning Biomedicine

![Architecture](./diagrams/architecture.png)

## Requirements

- Python 3.10 (optional, development should be done in the provided Docker environment)
- Docker Desktop (or Docker Compose)

## Build and run

Note: update the environment variables in `env` folder before running the following commands:

```shell
cd <project-folder>
docker-compose build
docker-compose up  # or "docker-compose up -d" to run in detached mode
```
Go to [http://localhost:8081](http://localhost:8081)

## Scaling

To scale up, add --scale web=N parameter. For example:

```shell
docker-compose up --scale web=3
```

## Add translations

```shell
poetry run python manage.py makemessages -l <language_code>
# edit and translate locale/<language_code>/LC_MESSAGES/django.po
```

## Development

Go in the web container by doing

`docker exec -it combio_web bash`

You can then run the following commands.

### CSS / Tailwind
1. `python manage.py tailwind install` - installs tailwind and its dependencies
2. `python manage.py tailwind start` - starts the tailwind watcher, which recompiles the css file on every change
3. `python manage.py collectstatic --no-input` - creates the static files, necessary to update the css file used by the templates

### Loading Dummy Test Data
1. `python manage.py loaddummydata` - loads dummy test data

### Elasticsearch Index update
1. `python manage.py search_index --rebuild` - rebuilds the elasticsearch index

### Create Superuser
1. `python manage.py createsuperuser` - creates a super user, necessary to access the admin area at /admin

### Dump & Load postgres data for local testing
On remote:
1. docker exec -i combio_db /bin/bash -c "PGPASSWORD=postgres pg_dump --username postgres --clean postgres" > ~/dump.sql
On your local:
2. scp combio.mpiwg-berlin.mpg.de:~/dump.sql ~/Desktop
To load on your local instance: 
4. docker exec -i combio_db /bin/bash -c "PGPASSWORD=postgres psql --username postgres postgres" < ~/Desktop/dump.sql
5. docker exec -i combio_web /bin/bash -c "python manage.py search_index --rebuild"

### Make Commands

Use the `Makefile` included for running different development tasks:

1. `make install` - installs the packages needed for development.
2. `make build` - build application.
3. `make start` - start application.
4. `make format` - runs `autoflake`, `isort` and `black` for fixing coding style.
5. `make lint` - runs `autoflake`, `isort`, `black`, `flake8` and `mypy` checks.
6. `make test` - run unit tests.
7. `make migrations` - generate migration scripts, if applicable.
8. `make migrate` - run migrations, if applicable.
9. `make superuser` - create superuser.
10. `make messages` - update messages.
11. `make compilemessages` - compile messages.
12. `make dumpdata` - backup data.
13. `make loaddata` - load data from backup.

