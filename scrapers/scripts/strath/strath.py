#!/usr/bin/env python
import sys
from pathlib import Path
import os
import re
import dateutil

sys.path.append("/code/")
os.environ["DJANGO_SETTINGS_MODULE"] = "project.settings"

import django

django.setup()
from combio_app.models import Record, Collection, BioTag
from combio_app.management.commands.utils import create_record

#### ^ setting up
import textract

COLLECTION_NAME = "Strathclyde University - Social Psychiatry Oral History Interviews"


def get_empty_metadata_dict():
    metadata = {
        "combio": {
            "title": "",
            "date": "",
            "collection": "",
            "permalink": "",
            "participants": [],
            "transcript": "",
        },
        "dc": {"title": "", "description": ""},
    }
    return metadata


import dateutil


def get_date_and_interviewed_person(text: str):
    match = re.search(r"^\s*([\w \t,]+)\s+(\d+(?:\s+|-)\w+\s+\d+)\s*$", text, flags=re.MULTILINE)
    if not match:
        assert False, "Date and interviewee not found"
    interviewed_person = match.group(1).strip(' ,')
    interview_date = match.group(2).strip(' ,')
    return interviewed_person, interview_date


def get_interviewer(text: str):
    match = re.search(r"^$\s*\w+\s+=\s+(\w+ \w+)\s*$", text, flags=re.MULTILINE)
    if not match:
        assert False, "Interviewer not found"
    interviewer = match.group(1)
    return interviewer


def get_metadata_from_transcript(text: str):
    lines = text.split("\n")
    first_5_lines = "\n".join(lines[:9])
    interviewed_person, interview_date = get_date_and_interviewed_person(first_5_lines)
    interview_date_obj = dateutil.parser.parse(interview_date)
    interviewer = get_interviewer(first_5_lines)
    metadata = get_empty_metadata_dict()
    metadata["combio"]["title"] = interviewed_person
    metadata["combio"]["date"] = interview_date_obj.strftime("%Y-%m-%d")
    metadata["combio"]["collection"] = COLLECTION_NAME  # ^ set outselves
    metadata["combio"]["permalink"] = "https://doi.org/10.15129/7ada0b9e-98eb-429d-98ac-486413ecf36a"
    metadata["combio"]["transcript"] = text
    metadata["combio"]["participants"] = [
        {"name": interviewer.strip(' ,'), "role": "interviewer"},
        {"name": interviewed_person.strip(' ,'), "role": "interviewee"},
    ]
    metadata["dc"]["title"] = interviewed_person
    metadata["dc"]["description"] = ""
    return metadata


def delete_previous_records():
    try:
        collection = Collection.objects.get(name=COLLECTION_NAME)
        for entry in collection.record_set.all():
            entry.delete()
    except:
        pass

if __name__ == "__main__":
    delete_previous_records()
    pdf_dir = (Path(__file__).parent / "../../strath_pdfs/").resolve()
    all_transcript_files = pdf_dir.glob("*.pdf")
    for transcript_file in all_transcript_files:
        transcript = textract.process(str(transcript_file)).decode("utf-8")
        metadata = get_metadata_from_transcript(transcript)
        create_record(metadata)
        # break
