import sys
from pathlib import Path
import os
import pandas as pd
from dateutil.parser import parse


sys.path.append("/code/")
os.environ["DJANGO_SETTINGS_MODULE"] = "project.settings"

import django

django.setup()
from combio_app.models import Record, Collection, BioTag

#### ^ setting up

def is_valid_date(date):
    if date:
        try:
            parse(date)
            return True
        except:
            return False
    return False

media_types = {
"Queen Mary History of Modern Biomedicine Interviews (Digital Collection)":"audio",
"Wellcome Trust Seminars":"text",
"NIH Oral Histories":"text",
"Science History Institute Oral Histories Collection":"audio",
"Samizdat Health Writer’s Co-operative Inc.":"text",
"Women in Medicine Oral Histories (Harvard Medical School. Joint Committee on the Status of Women)":"video",
"National Institute of Health - National Library of Medicine":"text",
"AHA Hospital Adminstration Oral History Collection":"text",
"Strathclyde University - Social Psychiatry Oral History Interviews":"text",
}

def restructure_metadata(original: dict, media_type: str):
    if not ("combio" in original and "dc" in original):
        if original["date"] == "":
            original["date"] = None
            print(original["date"])
        if original["date"] == "1998-00-00":
            original["date"] = "1998-01-01"
        if original["date"] == "2022-03-211":
            original["date"] = "2022-03-21"

        if not is_valid_date(original["date"]):
            original["date"] = None
        original["media_type"] = media_type
        return original
    new_metadata = {}
    new_metadata["title"] = original["combio"]["title"]
    if not "date" in original["combio"]:
        original["combio"]["date"] = None
    new_metadata["date"] = original["combio"]["date"]
    if new_metadata["date"] == '':
        new_metadata["date"] = None
    if not is_valid_date(new_metadata["date"]):
        new_metadata["date"] = None
    print(new_metadata['date'])
    new_metadata["collection"] = original["combio"]["collection"]
    new_metadata["permalink"] = original["combio"]["permalink"]
    new_metadata["participants"] = original["combio"]["participants"]
    new_metadata["transcript"] = original["combio"]["transcript"]
    new_metadata["description"] = original["dc"]["description"]
    new_metadata["media_type"] = media_type
    return new_metadata

def get_csv(filename: str):
    return pd.read_csv(filename)


def get_collection(collection_name: str):
    try:
        collection = Collection.objects.get(name=collection_name)
        return collection
    except:
        return None


def process_records_in_collection(collection: Collection, media_type: str):
    for entry in collection.record_set.all():
        entry.metadata = restructure_metadata(entry.metadata, media_type)
        entry.save()


if __name__ == "__main__":
    table = get_csv("./media_types.csv")
    for collection in Collection.objects.all():
            print(f"Processing collection: {collection}")
            media_type = media_types[collection.name]
            print(media_type)
            process_records_in_collection(collection, media_type)
