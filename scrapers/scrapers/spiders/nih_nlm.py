from urllib.parse import urljoin
import scrapy

from scrapy.http import Request

import textract


class NIHNLMSpider(scrapy.Spider):
    name = "nih_nlm"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = [
        "https://collections.nlm.nih.gov/displaystruct/nlm:nlmuid-100971572-ohset",
        "https://collections.nlm.nih.gov/displaystruct/nlm:nlmuid-101548136-ohset",
    ]

    def parse(self, response):
        months = (
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        )

        histories = response.xpath(".//span")
        for history in histories:
            item = {}
            metadata_dc = {}
            metadata_combio = {}
            intervieweeDateArray = history.extract().split("</a>")[1].split("<br/>")[0].split(",")
            date = ""
            interviewee = ""
            if len(intervieweeDateArray) == 3:
                print(response.url)
                if (
                    response.url
                    == "https://digirepo.nlm.nih.gov/dr-webapp/ajaxp?theurl=http://localhost:8080/fedora/get/nlm:nlmuid-100971572-ohset/DisplaySTRUCT"
                ):
                    date = intervieweeDateArray.pop()
                    date = date.strip().split(" ")
                    year = date[1]
                    month = months.index(date[0]) + 1
                    day = 1
                    date = f"{year}-{month:02d}-{day:02d}"
                    interviewee = ",".join(intervieweeDateArray)

                if (
                    response.url
                    == "https://digirepo.nlm.nih.gov/dr-webapp/ajaxp?theurl=http://localhost:8080/fedora/get/nlm:nlmuid-101548136-ohset/DisplaySTRUCT"
                ):
                    # date = intervieweeDateArray.pop()
                    # date = date.strip().split(" ")
                    # print(date)
                    year = intervieweeDateArray[2].strip()
                    monthDay = intervieweeDateArray[1].strip().split(" ")
                    print(monthDay)
                    month = months.index(monthDay[0]) + 1
                    day = monthDay[1]
                    if day == "8-10":
                        day = "8"
                    day = int(day)
                    date = f"{year}-{month:02d}-{day:02d}"
                    interviewee = intervieweeDateArray[0]

                metadata_combio["title"] = "Interview with " + interviewee
                metadata_combio["date"] = date
                metadata_combio["participants"] = []
                metadata_combio["participants"].append({"name": interviewee, "role": "interviewee"})
                # metadata_combio["participants"].append({"name": "David Healy", "role": "interviewer"})
                metadata_combio["collection"] = "National Institute of Health - National Library of Medicine"
                pdf_ext = history.xpath('.//a[contains(@href, "pdf")]/@href').get()
                metadata_combio["permalink"] = urljoin("https://collections.nlm.nih.gov", pdf_ext)
                metadata_dc["description"] = metadata_combio["title"]
                # metadata_combio["transcript"] = "lorem"
                item["combio"] = metadata_combio
                item["dc"] = metadata_dc
                # item["combio"]["transcript"] = ""
                # print(item["combio"]["date"])
                txt_ext = history.xpath('.//a[contains(@href, "txt")]/@href').get()
                txt_url = urljoin("https://collections.nlm.nih.gov", txt_ext)  # .replace(":", "\\:")
                # yield item
                yield scrapy.Request(txt_url, callback=self.get_transcript, cb_kwargs={"item": item})

    def get_transcript(self, response, item):
        # print(response)
        item["combio"]["transcript"] = (
            response.text
            # .decode("utf-8")
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
        )

        yield item
