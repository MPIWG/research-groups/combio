from urllib.parse import urljoin
import scrapy
# import textract


class NIHHHSSpider(scrapy.Spider):
    name = "nih_hhs"

    custom_settings = {  # this adds the pipeline to the spider (yield result to combio)
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = [
        "https://collections.nlm.nih.gov/displaystruct/nlm:nlmuid-101548136-ohset",
    ]

    def parse(self, response):
        months = (
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        )

        histories = response.xpath(".//span")
        for history in histories:
            item = {}
            intervieweeDateArray = history.extract().split(
                "</a>")[1].split("<br/>")[0].split(",")

            # special case for "AcademyHealth 25th Annual Research Meeting"
            if len(intervieweeDateArray) == 4:
                # remove the second element
                intervieweeDateArray.pop(1)
            interviewee = intervieweeDateArray[0]
            month_and_date = intervieweeDateArray[1].strip().split(" ")
            month = months.index(month_and_date[0]) + 1
            day = month_and_date[1].split("-")[0]
            year = intervieweeDateArray[-1].strip()
            date = f"{year}-{month:02d}-{int(day):02d}"

            item["title"] = "Interview with " + interviewee
            item["date"] = date
            item["participants"] = []
            item["participants"].append(
                {"name": interviewee, "role": "interviewee"})
            # item["participants"].append({"name": "David Healy", "role": "interviewer"})
            item["collection"] = "National Institute of Health - History of Health Services Oral History"
            pdf_ext = history.xpath('.//a[contains(@href, "pdf")]/@href').get()
            item["permalink"] = urljoin(
                "https://collections.nlm.nih.gov", pdf_ext)
            item["description"] = item["title"]

            # other misc. metadata
            item["media_type"] = ""
            item["summary"] = ""
            item["publisher"] = ""

            txt_ext = history.xpath('.//a[contains(@href, "txt")]/@href').get()
            txt_url = urljoin("https://collections.nlm.nih.gov",
                              txt_ext)  # .replace(":", "\\:")
            # yield item
            yield scrapy.Request(txt_url, callback=self.get_transcript, cb_kwargs={"item": item})

    def get_transcript(self, response, item):
        item["transcript"] = (
            response.text
            # .decode("utf-8")
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
        )

        yield item
