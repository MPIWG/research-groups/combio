from urllib.parse import urljoin
from urllib.parse import urlparse
import scrapy
from scrapy.http import Request
from collections import defaultdict

import os
from dataclasses import dataclass, field
from typing import Any, Callable

import requests
from requests.auth import HTTPBasicAuth
import re

import textract

# nlp = spacy.load("en_core_web_sm")


class SHISpider(scrapy.Spider):
    name = "shi"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = [
        "https://digital.sciencehistory.org/collections/vjikmuc.atom?page=1"]

    def parse(self, response):
        response.selector.remove_namespaces()
        histories = response.xpath(
            '//entry/link[contains(@type, "application/json")]/@href').getall()
        print(histories)
        for h in histories:
            url = h
            print(url)
            yield scrapy.Request(url, callback=self.parse_oral_history)

        next = response.xpath('//link[contains(@rel, "next")]/@href').get()
        try:
            yield scrapy.Request(next)
        except:
            print("looks like we are done here")

    def parse_oral_history(self, response):
        scrapedData = response.json()
        item = {}

        item["title"] = scrapedData["title"]

        # metadata_combio = defaultdict(list)
        # item["subjects"] = scrapedData["subject"]
        item["collection"] = "Science History Institute Oral Histories Collection"
        item["transcript"] = "temp transcript"
        item["description"] = scrapedData["description"]
        # item['transcript_text'] = "";
        # print(scrapedData['items'][0]['locations'][1]["license"]["label"])
        # locations = scrapedData['items'][0]['locations']
        # wtItemURL = None
        # license = None
        item["participants"] = []
        for creator in scrapedData["creator"]:
            if (
                (creator["category"] == "interviewer")
                or (creator["category"] == "interviewee")
                or (creator["category"] == "participant")
            ):
                clean_name = re.sub(",\s\d{4}\s*(.*)", "", creator["value"])
                clean_name = re.sub("\s\d{4}\s*(.*)", "", clean_name)
                item["participants"].append(
                    {"name": clean_name, "role": creator["category"]})

        # metadata_dc["places"] = []
        # for place in scrapedData["place"]:
        #    try:
        #        metadata["places"].append(place["value"])
        #    except:
        #        print("something about places missing")

        item["permalink"] = response.url.replace(".json", "")

        # metadata["other"] = metadata_other
        item["date"] = scrapedData["date_of_work"][0]["start"]

        # wtItemURL = scrapedData['items'][0]['locations'][1]["url"]
        pdfURL = scrapy.Request(item["permalink"])
        # yield item

        try:
            yield scrapy.Request(item["permalink"], callback=self.parseHTML, cb_kwargs={"item": item})
        except:
            print("no html here")
            yield item

    def parseHTML(self, response, item):
        parsed_uri = urlparse(response.url)
        domain = "{uri.scheme}://{uri.netloc}/".format(uri=parsed_uri)

        try:
            pdfURL = domain + "/" + \
                response.xpath(
                    '//a[contains(@href, "/downloads/orig/pdf")]/@href').get()
            yield scrapy.Request(pdfURL, callback=self.save_pdf, cb_kwargs={"item": item})
        except:
            print("no pdf here")
            yield item

    def save_pdf(self, response, item):
        # response.css("tr.ds-table-row"):
        path = "./shi_pdfs_public/" + \
            item["permalink"].split("/")[-1] + ".pdf"
        print(path)
        self.logger.info("Saving PDF %s", path)

        # this only needs to run the first time
        with open(path, "wb") as f:
            f.write(response.body)

        item["transcript"] = str(
            textract.process(path, encoding="unicode")
            .decode("utf-8")
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
        )

        yield item
