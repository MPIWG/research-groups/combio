from pathlib import Path

import scrapy


class StrathSpider(scrapy.Spider):
    name = "strath"

    def __init__(self, name=None, **kwargs):
        super().__init__(name, **kwargs)
        self.metadata = {
            "combio": {"title": "", "date": "", "collection": "", "permalink": "", "participants": []},
            "dc": {"title": "", "description": ""},
        }

    def start_requests(self):
        urls = [
            "https://pureportal.strath.ac.uk/en/datasets/social-psychiatry-oral-history-interviews",
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def closed(self, reason):
        self.log(self.metadata)

    def parse(self, response: scrapy.http.Response):
        title = response.css("div.introduction h1::text").get()
        self.metadata["title"] = title
        self.metadata["date"] = response.css("span.date").getall()[1]
        self.metadata["collection"] = ""
        self.metadata["permalink"] = response.css(
            "div.doi-link > a.link::attr(href)").get()
        self.metadata["participants"] = []
        self.metadata["description"] = response.css(
            "div.textblock::text").get()

        # page = response.url.split("/")[-2]
        # filename = f"quotes-{page}.html"
        # Path(filename).write_bytes(response.body)
        # self.log(f"Saved file {filename}")
