import re
import scrapy

class WashingtonMOHPSpider(scrapy.Spider):
    name = "washington_mohp"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = [
        "http://beckerexhibits.wustl.edu/oral/interviews/index.html",
    ]

    def parse(self, response: scrapy.http.Response):
        # get ul/li/a hrefs where ul class='longlist'
        for href in response.xpath("//ul[@class='longlist']/li/a/@href").getall():
            print(f"found href: {href}")
            yield response.follow(href, self.parse_interview_main_page)

    def parse_interview_main_page(self, response: scrapy.http.Response):
        all_metadata = {}
        # get interviewee name from the h1 text
        interviewee = response.xpath("//h1/text()").get()
        # get all the rows in the table with class="OHtable
        rows = response.xpath("//table[@class='OHtable']/tr")
        # * the first row contains the interviewer name (in the second td)
        interviewer = rows[0].xpath("td[2]/text()").get()
        # * the second row contains the date (in the second td)
        interview_date = rows[1].xpath("td[2]/text()").get()
        interview_date = self.process_date(interview_date)
        print(f"🟢interview_date: {interview_date}")
        # * the fifth and sixth rows contain the bio and summary. These are combined into just "summary"
        interview_bio = rows[4].xpath("td[2]/text()").get()
        interview_summary = rows[5].xpath("td[2]/text()").get()
        entire_summary = f"Biography: {interview_bio}\nInterview Summary: {interview_summary}"

        # collect all metadata
        all_metadata["title"] = f"Interview with {interviewee}"
        all_metadata["participants"] = [
            {"name": interviewee, "role": "interviewee"},
            {"name": interviewer, "role": "interviewer"},
        ]
        all_metadata["description"] = entire_summary
        all_metadata["date"] = interview_date
        all_metadata["collection"] = "Washington University School of Medicine Oral History Project"
        all_metadata["permalink"] = response.url
        all_metadata["summary"] = "" # ! to be filled in later
        all_metadata["media_type"] = "text"
        all_metadata["publisher"] = "Washington University School of Medicine"
        
        # * get the first href in the table with class="OHtable4"
        # * this is the link to the transcript
        transcript_href = response.xpath("//table[@class='OHtable4']/tr/th/a/@href").get()
        yield response.follow(transcript_href, self.parse_transcript, cb_kwargs={"metadata": all_metadata})

    def parse_transcript(self, response: scrapy.http.Response, metadata: dict):
        # get all text in the table with class="transtable"
        transcript = response.xpath("//table[@class='transtable']/tr/td/p/em/text()").getall()
        transcript = " ".join(transcript)
        metadata["transcript"] = transcript
        yield metadata

    @staticmethod
    def process_date(date:str):
        months = (
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        )
        # example: "March 1, 2021"
        re_match = re.match(r"(\w+) (\d+), (\d+)", date)
        if re_match:
            month, day, year = re_match.groups()
            month = months.index(month) + 1
            return f"{year}-{month:02d}-{int(day):02d}"
        else:
            return None

        