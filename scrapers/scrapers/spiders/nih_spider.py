from urllib.parse import urljoin
import scrapy

from scrapy.http import Request

import textract


class NIHSpider(scrapy.Spider):
    name = "nih"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = ["https://history.nih.gov/display/history/NIH+Oral+Histories/"]

    def parse(self, response):
        # products = response.xpath("//*[contains(@class, 'ph-summary-entry-ctn')]/a/@href").extract()
        histories = response.xpath('//a[contains(@href, "https://history.nih.gov/display/history/")]/@href').getall()
        for h in histories:
            print(h)
            url = urljoin(response.url, h)
            yield scrapy.Request(url, callback=self.parse_oral_history)

    def parse_oral_history(self, response):
        item = {}
        metadata_dc = {}
        metadata_combio = {}

        metadata_combio["title"] = response.css("title::text").get()
        metadata_combio["date"] = None
        metadata_dc["description"] = metadata_combio["title"]
        metadata_combio["permalink"] = response.url
        metadata_combio["collection"] = "NIH Oral Histories"
        metadata_combio["participants"] = []

        # href = response.xpath('//a[contains(@href, "/download/attachments/")]/@href').get();
        href = response.xpath('//a[contains(@href, ".pdf")]/@href').get()
        pdfURL = response.urljoin(href)
        print(pdfURL)

        item["combio"] = metadata_combio
        item["dc"] = metadata_dc

        # yield item
        yield scrapy.Request(pdfURL, callback=self.save_pdf, cb_kwargs={"item": item})

    def save_pdf(self, response, item):
        print("woooohoooooo!!!!")
        path = response.url.split("/")[-1]
        self.logger.info("Saving PDF %s", path)
        name = path.split("?")[0].replace("%20", "_").replace("%2C", "")
        with open("nih_pdfs/" + name, "wb") as f:
            f.write(response.body)
        item["combio"]["transcript"] = str(
            textract.process("nih_pdfs/" + name)
            .decode("utf-8")
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
        )
        yield item
