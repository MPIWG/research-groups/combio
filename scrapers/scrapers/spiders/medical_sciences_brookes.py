import logging
import pathlib
import re
import scrapy

from io import StringIO

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage


class MedicalSciencesBrookes(scrapy.Spider):
    name = "medical_sciences_brookes"
    collection_name = "Medical Sciences Video Archive"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    # The Oxford Brookes repository uses the OAI-PMH protocol.
    base_url = "https://radar.brookes.ac.uk/radar/oai"

    # ID of the set 'archives' which contains the oral history collection
    archive_set = "50c0f666-49e7-4c6e-857d-72652e9dd097"
    archive_url = ("https://www.brookes.ac.uk/library/special-collections/"
                   "public-and-allied-health/medical-sciences-video-archive/")

    # Our start URL is a request to list all records in the set.
    # Note that this returns a truncated list.
    # The metadata format we'll use is "oai_datacite" - they also offer
    # Dublin Core ("oai_dc"), but this does not contain all the data.
    start_urls = [f"{base_url}?set={archive_set}"
                  "&verb=ListRecords&metadataPrefix=oai_datacite"]

    def parse(self, response):
        """Parse a list of records, then request the next batch."""
        if r := response.xpath("//error/text()").get():
            self.logger.error(r)
            return

        response.selector.remove_namespaces()

        for record in response.xpath("//record"):
            relatedIdentifier = record.xpath(".//relatedIdentifier"
                                             "[@relatedIdentifierType='URL']"
                                             "/text()").get()
            if relatedIdentifier == self.archive_url:
                html_link = record.xpath(".//alternateIdentifier/text()").get()
                if not html_link:
                    html_link = record.xpath("///identifier"
                                             "[@identifierType='URL']"
                                             "/text()").get()
                yield scrapy.Request(html_link,
                                     callback=self.parse_record,
                                     cb_kwargs={"record": record})

        # Continue with the next batch, if any.
        r = response.xpath("//resumptionToken")
        if token := r.xpath(".//text()").get():
            position = int(r.attrib["cursor"])
            length = int(r.attrib["completeListSize"])
            percentage = position / length * 100
            self.logger.info(f"At list position {position} of {length}. "
                             f"{percentage:.2f}% done.")
            # The resumption request continues the same ListRecords request.
            # It thus does not take any 'set' or 'metadataPrefix' property.
            yield scrapy.Request(f"{self.base_url}"
                                 f"?verb=ListRecords&"
                                 f"resumptionToken={token}")
        else:
            self.logger.info("Parsed the complete list of records.")

    def parse_record(self, response, record):
        """Parse a record of an oral history interview."""

        def get_entry(id, getall=False):
            xpath = f".//{id}/text()"
            if getall:
                entry = record.xpath(xpath).getall()
            else:
                entry = record.xpath(xpath).get()
            return entry

        title = get_entry("title")
        abstract = get_entry("description[@descriptionType='Abstract']") or ""

        # Try to fetch the original date from HTML, else use metadata entry.
        date = self.parse_date(response, get_entry("date"))

        participants = get_entry("creatorName", getall=True)

        if re.search(r"interview|conversation", title, re.IGNORECASE):
            # The interviewer is listed last.
            interviewees = participants[0:-1]
            interviewer = participants[-1]
            participants = [{"name": name, "role": "interviewee"}
                            for name in interviewees]
            participants.append({"name": interviewer, "role": "interviewer"})
        else:
            participants = [{"name": name, "role": "participant"}
                            for name in participants]

        metadata_dc = {"title": title,
                       "description": abstract}

        metadata_combio = {"title": title,
                           "date": date,
                           "collection": self.collection_name,
                           "permalink": response.url,
                           "participants": participants}

        item = {"combio": metadata_combio,
                "dc": metadata_dc}

        yield scrapy.Request((f"{self.base_url}?"
                              f"&verb=GetRecord&metadataPrefix=oai_dc"
                              f"&identifier={get_entry('identifier')}"),
                             callback=self.get_pdf,
                             cb_kwargs={"item": item})

    def parse_date(self, response, default):
        """Parse the original date, if any, from HTML record.
        Sadly, this information is not included in any OAI-PMH metadata."""
        dates = response.xpath("//h3[contains(.,'Dates')]/"
                               "following-sibling::p[1]/text()").get()
        original_date = re.search(r"Original artefact: "
                                  r"(.*?)(<br\/?>|$)", dates)
        date = original_date[1].strip() if original_date else default

        # In 99% of cases, only years are given, so add default month and day.
        if len(date) <= 4:
            return f"{date}-01-01"
        else:
            return date

    def get_pdf(self, response, item):
        """Fetch PDF link from DublinCore record, go on to save it."""
        files = response.xpath("//*[name()='dc:identifier']/text()").getall()
        try:
            path = next(path for path in files if path.endswith(".pdf"))
            yield scrapy.Request(path,
                                 callback=self.save_pdf,
                                 cb_kwargs={"item": item})
        except StopIteration:
            self.logger.warning(f"No PDF file found for '{response.url}'. "
                                "Inserting empty transcript.")
            item["combio"]["transcript"] = ""
            yield item

    def save_pdf(self, response, item):
        """Save the PDF and extract the text."""
        self.logger.info(f"Saving PDF {response.url}...")

        folder = self.name + "_pdfs/"
        pathlib.Path(folder).mkdir(exist_ok=True)

        name = response.url.split("/")[-1].replace(",", "")
        save_location = folder + name
        with open(save_location, "wb") as f:
            f.write(response.body)

        self.logger.info(f"Processing transcript {name}.")

        # Most PDFs in the collection have a flag set to disable extraction.
        # To circumvent, we need to call PDFMiner directly, with
        # 'check_extractable' set to False.
        # PDFMiner logs A LOT, so I disabled all logs (<INFO) here (not ideal).
        logging.disable(logging.INFO)
        output_string = StringIO()
        with open(save_location, 'rb') as f:
            rsrcmgr = PDFResourceManager(caching=True)
            device = TextConverter(rsrcmgr, output_string, laparams=LAParams())
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            for page in PDFPage.get_pages(f, set(), maxpages=0, caching=True,
                                          check_extractable=False):  # no check
                interpreter.process_page(page)
        # Enable logs again.
        logging.disable(logging.NOTSET)

        item["combio"]["transcript"] = str(
            output_string.getvalue()
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
            )
        return item
