import re
import scrapy


class ColdSpringHarborLaboratory(scrapy.Spider):
    name = "cold_spring_harbor_laboratory"

    collection_name = ("Cold Spring Harbor Laboratory "
                       "Oral History Collection")

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    base_url = "https://library.cshl.edu"
    start_urls = [base_url + "/oralhistory/"]

    def parse(self, response):
        """Parse the main page to get all scientists."""

        scientists_xpath = "//div[@id='interviews']//a/@href"
        for scientist in response.xpath(scientists_xpath).getall():
            url = self.base_url + scientist
            yield scrapy.Request(url, callback=self.parse_scientist)

        self.logger.info("Fetched all scientist pages.")

    def parse_scientist(self, response):
        """Parse a scientist page to get all topics."""

        topic_xpath = "//div[@id='scientists_speaking_wide']//a/@href"
        for topic in response.xpath(topic_xpath).getall():
            url = self.base_url + topic
            yield scrapy.Request(url, callback=self.parse_topic)

        scientist = response.xpath("//div[@id='fulldescription']"
                                   "/h1/text()").get()
        self.logger.info(f"Fetched all topics for {scientist}.")

    def parse_date(self, str):
        months = ("Jan", "Feb", "Mar", "Apr",
                  "May", "Jun", "Jul", "Aug", "Sep",
                  "Oct", "Nov", "Dec")

        year = re.search(r"\b(\d{4})\b", str)
        day = re.search(r"\b(\d{1,2})\b", str)
        month = re.search("|".join(months), str)

        year = year[0] if year else "1900"
        day = int(day[0]) if day else 1
        month = months.index(month[0]) + 1 if month else 1

        return f"{year}-{month:02d}-{day:02d}"

    def parse_topic(self, response):
        """Parse an item page of an oral history snippet or 'topic'."""

        title = response.xpath("//div[@id='header_bar']//text()").getall()
        title = "".join(title).strip()

        participants = []
        interviewee = response.xpath("//div[@id='header_bar']"
                                     "/span[@class='speaker']"
                                     "/text()").get()

        if interviewee == "Klar & Strathern":
            participants.append({"name": "Amar Klar",
                                 "role": "interviewee"})
            participants.append({"name": "Jeff Strathern",
                                 "role": "interviewee"})
        else:
            participants.append({"name": interviewee,
                                 "role": "interviewee"})

        # The snippets are so short that they don't have abstracts.
        abstract = ""

        date = response.xpath("//div[@id='transcript_text']"
                              "/div[@class='print']"
                              "/text()").get()
        date = self.parse_date(date)

        # The transcript is directly available in HTML.
        transcript = response.xpath("//div[@id='transcript_text']"
                                    "//text()").getall()
        transcript = " ".join(transcript).strip() \
                                         .replace("\n", " ") \
                                         .replace("\f", " ") \
                                         .replace("\t", " ")

        # Insert metadata.
        all_metadata = {"title": title,
                        "date": date,
                        "collection": self.collection_name,
                        "permalink": response.url,
                        "participants": participants,
                        "description": f"Interview with {interviewee}.",
                        "media_type": "text",
                        "summary": abstract, # ! To be filled in later
                        "publisher": "Cold Spring Harbor Laboratory",
                        "transcript": transcript,
                       }
        
        # self.logger.info(f"Scraped metadata and transcript for '{title}'.")

        # self.logger.info(all_metadata)
        return all_metadata 
