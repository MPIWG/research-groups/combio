from urllib.parse import urljoin
import pathlib
import re
import scrapy
import textract


class Aha(scrapy.Spider):
    name = "aha"

    collection_name = ("AHA Hospital Adminstration "
                       "Oral History Collection")

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    base_url = "https://www.aha.org"
    start_urls = [base_url + ("/oral-history-project/"
                              "2006-01-13-hospital-administration-"
                              "oral-history-collection")]

    def parse(self, response):
        """Parse the complete list of records."""

        item_xpath = "//table//a"
        for item in response.xpath(item_xpath):
            url = item.attrib["href"]
            if url.startswith("/videos"):
                continue
            str = item.xpath("text()").get()
            r = re.search(r"(.*?)($|:|,? M\.D\.|, D\.P\.H\.|\((\d{4})\))", str)
            interviewee = r[1]
            year = item.xpath("following-sibling::text()").get().strip()[1:-1]
            if not year:
                year = r[3]
            self.logger.info(interviewee)
            self.logger.info(year)
            cb = self.parse_pdf if url.endswith(".pdf") else self.parse_record
            yield scrapy.Request(urljoin(self.base_url, url),
                                 callback=cb,
                                 cb_kwargs={"interviewee": interviewee,
                                            "year": year})

        self.logger.info("Fetched all record pages.")

    def parse_date(self, str):
        months = ("January", "February", "March", "April",
                  "May", "June", "July", "August", "September",
                  "October", "November", "December")

        year = re.search(r"\b(\d{4})\b", str)
        day = re.search(r"\b(\d{1,2})\b", str)
        month = re.search("|".join(months), str)

        year = year[0] if year else "1900"
        day = int(day[0]) if day else 1
        month = months.index(month[0]) + 1 if month else 1

        return f"{year}-{month:02d}-{day:02d}"

    def parse_record(self, response, interviewee, year):
        """Parse an item page of an oral history interview."""

        title = response.xpath("//h1/span[@class='title']/text()").get()
        desc = response.xpath("//section[@class='content']/"
                              "div[@class='body']//text()").getall()
        desc = "\n".join(desc).strip()

        participants = []
        participants.append({"name": interviewee,
                             "role": "interviewee"})

        date = year
        if r := re.search(r"interviewed by (.*?)([\n,_]|on (.*))",
                          desc, re.IGNORECASE):
            participants.append({"name": r[1].strip(),
                                 "role": "interviewer"})
            date = r[3] or year

        date = self.parse_date(date)

        # Insert metadata.

        metadata_dc = {"title": title,
                       "description": self.cleanup(desc)}

        metadata_combio = {"title": title,
                           "date": date,
                           "collection": self.collection_name,
                           "permalink": response.url,
                           "participants": participants}

        item = {"combio": metadata_combio,
                "dc": metadata_dc}

        self.logger.info(f"Scraped metadata for '{title}'.")

        pdf_url = response.xpath("//div[@class='field_media_file']"
                                 "//a[@type='application/pdf']/@href").get()

        yield scrapy.Request(urljoin(self.base_url, pdf_url),
                             callback=self.save_pdf,
                             cb_kwargs={"item": item})

    def parse_pdf(self, response, interviewee, year):
        participants = []
        participants.append({"name": interviewee,
                             "role": "interviewee"})

        title = f"{interviewee} Oral History {year}"

        metadata_dc = {"title": title,
                       "description": ""}

        metadata_combio = {"title": title,
                           "date": self.parse_date(year),
                           "collection": self.collection_name,
                           "permalink": response.url,
                           "participants": participants}

        item = {"combio": metadata_combio,
                "dc": metadata_dc}

        yield self.save_pdf(response, item)

    def cleanup(self, str):
        return str.replace("\n", " ") \
                  .replace("\f", " ") \
                  .replace("\t", " ")

    def save_pdf(self, response, item):
        title = item["combio"]["title"]

        if pathlib.Path(response.url).suffix != ".pdf":
            self.logger.warning(f"No PDF file found for '{title}'. "
                                "Inserting empty transcript.")
            item["combio"]["transcript"] = ""
            return item

        self.logger.info(f"Saving PDF {response.url}...")

        folder = self.name + "_pdfs/"
        pathlib.Path(folder).mkdir(exist_ok=True)

        name = response.url.split("/")[-1]
        save_location = folder + name
        with open(save_location, "wb") as f:
            f.write(response.body)

        self.logger.info(f"Processing transcript {name}.")
        transcript = textract.process(save_location).decode("utf-8")
        item["combio"]["transcript"] = self.cleanup(transcript)
        return item
