#!/usr/bin/env python
import sys
from pathlib import Path
import os
import re
from langchain.prompts import PromptTemplate
from langchain.output_parsers import ResponseSchema, StructuredOutputParser
from langchain.llms import OpenAI

with open("/code/env/openai.txt") as f:
    openai_key = f.read()
    os.environ["OPENAI_API_KEY"] = openai_key


model_name = "text-davinci-003"
temperature = 0.0

model = OpenAI(model_name=model_name, temperature=temperature, max_tokens=1500)
COLLECTION_NAME = "Strathclyde University - Social Psychiatry Oral History Interviews"

#### ^ setting up

description_schema = ResponseSchema(name="description", description="a summary of the provided transcript")
response_schemas = [description_schema]
output_parser = StructuredOutputParser.from_response_schemas(response_schemas)
format_instructions = output_parser.get_format_instructions()

template_string = """You are highly intelligent and accurate data extractor from plain text input. \Based on the contents of this string, delimited by triple brackets: ```{transcript}```, which are the first sentences of an interview transcript.\ {format_instructions}"""

prompt_obj = PromptTemplate.from_template(template_string)

####
import scrapy
import re


class NIH_NLM_PrimaryCareSpider(scrapy.Spider):
    name = "nih_nlm_primarycare"

    URL_ROOT = "https://collections.nlm.nih.gov"
    COLLECTION_NAME = "NIH NLM - Primary Care Oral History Collection"
    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    def __init__(self, name=None, **kwargs):
        super().__init__(name, **kwargs)

    @staticmethod
    def get_empty_metadata_dict():
        metadata = {
                "title": "",
                "date": "",
                "collection": "",
                "permalink": "",
                "participants": [],
                "transcript": "",
                "description": "",
        }
        return metadata

    def start_requests(self):
        # note: you can't use https://collections.nlm.nih.gov/catalog/nlm:nlmuid-101166350-ohset
        #       because the interviews are loaded after the initial request is fetched.
        # return
        urls = [
            "https://collections.nlm.nih.gov/displaystruct/nlm:nlmuid-101166350-ohset",
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def closed(self, reason):
        pass

    @staticmethod
    def process_date(match: re.Match):
        month = int(match.group(1))
        day = int(match.group(2))
        year = int(match.group(3))
        return f"{year:04d}-{month:02d}-{day:02d}"

    def parse(self, response: scrapy.http.Response):
        interviews = response.xpath('//li[@class="tree"]/span')
        for interview in interviews:
            metadata = self.get_empty_metadata_dict()
            # fill in metadata
            person_and_date = interview.xpath("text()").get()
            interviewee, date = person_and_date.strip().rsplit(",", 1)  # split from the right once
            interviewee = interviewee.strip()
            date = re.sub(r"(\d+)/(\d+)/(\d+)", self.process_date, date.strip())
            txt_url = self.URL_ROOT + interview.xpath("a[last()]/@href").get()
            interviewer = "Mullan, Fitzhugh"
            metadata["title"] = interviewee
            metadata["date"] = date
            metadata["collection"] = self.COLLECTION_NAME  # ^ set outselves
            metadata["permalink"] = txt_url
            metadata["participants"] = [
                {"name": interviewer, "role": "interviewer"},
                {"name": interviewee, "role": "interviewee"},
            ]
            metadata["title"] = interviewee
            metadata["description"] = ""
            metadata["transcript"] = "meow"
            metadata["media_type"] = "audio"

            # & URL of transcript
            yield scrapy.Request(
                txt_url, callback=self.get_transcript, cb_kwargs={"interview_html": interview, "metadata": metadata}
            )

    def get_transcript(self, response, interview_html, metadata: dict):
        """uses OpenAI to get transcript."""

        transcript = response.text  # entire transcript as string
        metadata["transcript"] = transcript
        # prompt = prompt_obj.format(transcript=transcript[:2500], format_instructions=format_instructions)
        # output = model(prompt)
        # description_dict = output_parser.parse(output)
        # description = description_dict["description"]
        # metadata["summary"] = description
        yield metadata
