from urllib.parse import urljoin
import scrapy

from scrapy.http import Request

import textract


class SamizdatSpider(scrapy.Spider):
    name = "samizdat"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = ["https://samizdathealth.org/shipwreck-references-healy/"]

    def parse(self, response):
        histories = response.xpath(
            '//a[contains(@href, "https://samizdathealth.org/wp-content/uploads/")]/@href'
        ).getall()
        for h in histories:
            item = {}

            metadata = {}
            metadata["date"] = None
            entry = response.xpath('//a[contains(@href, "' + h + '")]/../..')
            interviewee = entry.xpath(
                ".//strong/text()").re_first(r"\xa0\s*(.*)").title()
            title = entry.xpath(".//a/text()").get()
            metadata["title"] = "Interview with " + interviewee + ": " + title
            metadata["participants"] = []
            metadata["participants"].append(
                {"name": interviewee, "role": "interviewee"})
            metadata["participants"].append(
                {"name": "David Healy", "role": "interviewer"})
            metadata["collection"] = "Samizdat Health Writer’s Co-operatrive Inc."
            metadata["permalink"] = h
            metadata["description"] = metadata["title"]
            item = metadata
            # url = urljoin(response.url, h)
            yield scrapy.Request(h, callback=self.save_pdf, cb_kwargs={"item": item})

    def save_pdf(self, response, item):
        print("woooohoooooo!!!!")
        path = response.url.split("/")[-1]
        self.logger.info("Saving PDF %s", path)
        name = path.split("?")[0].replace("%20", "_").replace("%2C", "")
        with open("samizdat_pdfs/" + name, "wb") as f:
            f.write(response.body)
        item["transcript"] = (
            str(textract.process("samizdat_pdfs/" +
                name).decode("utf-8").replace("\n", " "))
            .replace("\f", " ")
            .replace("\t", " ")
        )

        yield item
