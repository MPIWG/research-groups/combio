from urllib.parse import urljoin
import scrapy
from scrapy.http import Request
from scrapy import crawler
import datetime
import dateparser
import re
import textract


class QueenMarySpider(scrapy.Spider):
    name = "queen_mary"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    def start_requests(self):
        urls = [
            "https://qmro.qmul.ac.uk/xmlui/handle/123456789/12359/browse?rpp=100&sort_by=2&type=dateissued&offset=0&etal=-1&order=ASC"
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        histories = response.css("div.artifact-description a::attr(href)").getall()

        for h in histories:
            #     print(h)
            url = urljoin(response.url, h) + "?show=full"
            yield scrapy.Request(url, callback=self.parse_oral_history)
            # interrupt scrape
            # raise scrapy.exceptions.CloseSpider(reason="first loop")

    def parse_oral_history(self, response):
        item = {}
        # id = eCheck.json()[0]["_id"]["$oid"]
        # print(id)

        item = {}
        metadata_dc = {}
        metadata_combio = {}
        metadata_dc["contributors"] = []
        metadata_combio["participants"] = []
        metadata_combio["collection"] = "Queen Mary"
        metadata_combio["transcript"] = "temp transcript"
        for index, data in enumerate(response.css("tr.ds-table-row")):
            field = data.css("td.label-cell::text").get().split(".")
            if len(field) == 2:
                field = field[1]
            else:
                field = field[1] + " (" + field[2] + ")"
            val = data.css("td.word-break::text").get()
            # yield {
            if field == "title":
                metadata_combio["title"] = val
            elif field == "identifier (uri)":
                metadata_combio["permalink"] = val
            elif field == "rights":
                metadata_dc["license"] = val
            elif field == "description (abstract)":
                metadata_dc["description"] = val
            elif field == "date (issued)":
                if val:
                    date_object = datetime.datetime.strptime(val, "%d/%m/20%y")
                    issued_date = date_object.strftime("%Y-%m-%d")
            else:
                if field == "contributor":
                    # print("###########CONTRIBUTORS ARRAY##############")
                    metadata_dc["contributors"].append(val)
                    if index == 0:
                        metadata_combio["participants"].append({"name": val, "role": "interviewer"})
                    if index == 1:
                        metadata_combio["participants"].append({"name": val, "role": "interviewee"})
                else:
                    metadata_dc[field] = [val]

                # url=response.urljoin(href),
                # callback=self.save_pdf
            # }

        # parsing date for combio date
        regex_result = re.search(r"\(([^)]+)\)", metadata_combio["title"])
        if regex_result:
            raw_date = regex_result.group(1)
            clean_raw_date = raw_date[len(raw_date) - 11 :]
            date_object = dateparser.parse(clean_raw_date)
            print(date_object)
            metadata_combio["date"] = date_object.strftime("%Y-%m-%d")
        else:
            metadata_combio["date"] = issued_date

        # raise scrapy.exceptions.CloseSpider(reason="first loop")

        pdfURL = response.css("meta[content*=pdf]::attr(content)").get()
        metadata_dc["pdf"] = pdfURL.split("/")[-1].split("?")[0]
        item["dc"] = metadata_dc
        metadata_combio["collection"] = "Queen Mary History of Modern Biomedicine Interviews (Digital Collection)"
        item["combio"] = metadata_combio
        # item['transcript_text'] = "";

        yield scrapy.Request(pdfURL, callback=self.save_pdf, cb_kwargs={"item": item})

    def save_pdf(self, response, item):
        # response.css("tr.ds-table-row"):
        path = "./queen_mary_pdfs/" + response.url.split("/")[-1].split("?")[0]
        print(path)
        self.logger.info("Saving PDF %s", path)
        # name = path.split('?')[0].replace('%20', '_').replace('%2C', '')
        with open(path, "wb") as f:
            f.write(response.body)

        # doc = pdf_reader(path, nlp);
        # item['transcript_text'] = doc.text;

        item["combio"]["transcript"] = str(
            textract.process(path, encoding="unicode").decode("utf-8").replace("\n", " ").replace("\f", " ")
        )

        yield item

        # yield scrapy.Request(pdfURL, callback=self.save_pdf, cb_kwargs={'item': item})
