import pathlib
import re
import scrapy
import textract


class WomenInMedicine(scrapy.Spider):
    name = "women_in_medicine"

    collection_name = ("Women in Medicine Oral Histories "
                       "(Harvard Medical School. "
                       "Joint Committee on the Status of Women)")

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    base_url = "https://collections.countway.harvard.edu"
    start_urls = [base_url + "/onview/items/browse?collection=12&page=1"]

    def parse(self, response):
        """Parse a page in the browse-items view of the collection."""

        item_xpath = "//div[@class='item record']/h2/a/@href"
        for item in response.xpath(item_xpath).getall():
            url = self.base_url + item
            yield scrapy.Request(url, callback=self.parse_oral_history)

        # Go on to the next page, if any.
        next_page_xpath = "//li[@class='pagination_next']/a[@rel='next']/@href"
        if next_page := response.xpath(next_page_xpath).get():
            yield scrapy.Request(self.base_url + next_page)
        else:
            self.logger.info("No more pages to parse in browse-items view.")

    def parse_date(self, str):
        months = ("January", "February", "March", "April",
                  "May", "June", "July", "August", "September",
                  "October", "November", "December")

        year = re.search(r"\b(\d{4})\b", str)
        day = re.search(r"\b(\d{1,2})\b", str)
        month = re.search("|".join(months), str)

        year = year[0] if year else "1900"
        day = int(day[0]) if day else 1
        month = months.index(month[0]) + 1 if month else 1

        return f"{year}-{month:02d}-{day:02d}"

    def clean_name(self, name):
        """Remove date of birth and death from name."""
        return re.search(r"^(.+?)(?=, \d+|$)", name)[0]

    def parse_oral_history(self, response):
        """Parse an item page of an oral history interview."""

        def get_entry(id, default=None, getall=False):
            xpath = (f"//div[@id='{id}']"
                     "/div[@class='element-text']"
                     "/text()")
            if getall:
                entry = response.xpath(xpath).getall()
            else:
                entry = response.xpath(xpath).get()
            if not entry and default is not None:
                self.logger.warning(f"No entry found for {id} "
                                    f"in {response.url}. "
                                    "Inserting default value")
                return default
            return entry

        title = get_entry("dublin-core-title")
        # Remove media description at the end of the title.
        title_regex = r"(.*?)(?=( \((audio|video|transcript))|$)"
        title = re.search(title_regex, title)[1]

        # TV shows etc.
        if not re.search(r"interview with (.*)", title, re.IGNORECASE):
            self.logger.info("Entry doesn't seem to be an interview: "
                             f"'{title}'. ")

        participants = []
        interviewee = get_entry("oral-history-item-type-metadata-interviewee")
        interviewer = get_entry("oral-history-item-type-metadata-interviewer")

        if interviewer:
            participants.append({"name": self.clean_name(interviewer),
                                 "role": "interviewer"})

        if interviewee:
            participants.append({"name": self.clean_name(interviewee),
                                 "role": "interviewee"})
        else:
            self.logger.warning(f"No interviewees found for '{title}'. "
                                "Using unspecified participants instead.")
            if not (parts := get_entry("dublin-core-contributor", None, True)):
                parts = get_entry("dublin-core-creator", None, True)
            for p in parts:
                participants.append({"name": self.clean_name(p),
                                     "role": "participant"})

        abstract = get_entry("dublin-core-abstract", "")
        date = self.parse_date(get_entry("dublin-core-date-created", ""))

        # Insert metadata.

        metadata_dc = {"title": title,
                       "description": abstract}

        metadata_combio = {"title": title,
                           "date": date,
                           "collection": self.collection_name,
                           "permalink": response.url,
                           "participants": participants}

        item = {"combio": metadata_combio,
                "dc": metadata_dc}

        self.logger.info(f"Scraped metadata for '{title}'.")

        pdf_url = response.xpath("//a[@class='download-file']/@href").get()
        yield scrapy.Request(pdf_url,
                             callback=self.save_pdf,
                             cb_kwargs={"item": item})

    def save_pdf(self, response, item):
        title = item["combio"]["title"]

        if pathlib.Path(response.url).suffix != ".pdf":
            self.logger.warning(f"No PDF file found for '{title}'. "
                                "Inserting empty transcript.")
            item["combio"]["transcript"] = ""
            return item

        self.logger.info(f"Saving PDF {response.url}...")

        folder = self.name + "_pdfs/"
        pathlib.Path(folder).mkdir(exist_ok=True)

        name = response.url.split("/")[-1]
        save_location = folder + name
        with open(save_location, "wb") as f:
            f.write(response.body)

        self.logger.info(f"Processing transcript {name}.")
        item["combio"]["transcript"] = (
            textract.process(save_location)
            .decode("utf-8").replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
            )
        return item
