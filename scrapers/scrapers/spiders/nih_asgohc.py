from urllib.parse import urljoin
import re
import scrapy
# import textract


class NIH_ASGOHCSpider(scrapy.Spider):
    name = "nih_asgohc"

    custom_settings = {
        "ITEM_PIPELINES": {
            "scrapers.pipelines.ComBioScrapersPipeline": 300,
        }
    }

    start_urls = [
        "https://collections.nlm.nih.gov/displaystruct/nlm:nlmuid-101318026-ohset",
    ]

    def parse(self, response):
        months = (
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        )

        histories = response.xpath(".//span")
        for history in histories:
            item = {}
            intervieweeDateStr = history.extract().split("</a>")[1].split("<br/>")[0]
            if interviewee_date_matches := re.match(r"(.*?),\s+(\w+) (\d+),\s+(\d+)", intervieweeDateStr):
                interviewee = interviewee_date_matches.group(1)
                month = interviewee_date_matches.group(2)
                day = interviewee_date_matches.group(3)
                year = interviewee_date_matches.group(4)
                date = f"{year}-{months.index(month) + 1:02d}-{int(day):02d}"
                self.log(f"🟢interviewee: {interviewee}, date: {date}")
            else:
                # luckily there's only one exception, and the correct date is August 20, 2004
                self.log(f"🟡couldn't match {intervieweeDateStr}")
                interviewee = intervieweeDateStr.split(",")[0] # this should be "Jane McLaughlin"
                date = "2004-08-20"

            self.log(f"🟢🟢interviewee: |{interviewee}|, date: {date}")

            item["title"] = "Interview with " + interviewee
            item["date"] = date
            item["participants"] = []
            item["participants"].append({"name": interviewee, "role": "interviewee"})
            # item["participants"].append({"name": "David Healy", "role": "interviewer"})
            item["collection"] = "National Institute of Health - Albert Szent-Györgyi oral history collection"
            pdf_ext = history.xpath('.//a[contains(@href, "pdf")]/@href').get()
            item["permalink"] = urljoin("https://collections.nlm.nih.gov", pdf_ext)
            item["description"] = item["title"]

            # other misc. metadata
            item["media_type"] = "text"
            item["summary"] = ""
            item["publisher"] = ""

            txt_ext = history.xpath('.//a[contains(@href, "txt")]/@href').get()
            txt_url = urljoin("https://collections.nlm.nih.gov", txt_ext)  # .replace(":", "\\:")
            item["transcript"] = None # to be filled in later
            yield scrapy.Request(txt_url, callback=self.get_transcript, cb_kwargs={"item": item})

    def get_transcript(self, response, item):
        item["transcript"] = (
            response.text
            # .decode("utf-8")
            .replace("\n", " ")
            .replace("\f", " ")
            .replace("\t", " ")
        )

        yield item