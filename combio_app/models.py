# from django.db.models import JSONField
from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.conf import settings
import json
import os
from pathlib import Path
from django_jsonform.models.fields import JSONField
from simple_history.models import HistoricalRecords

BASE_DIR = Path(__file__).resolve().parent.parent

with open(
    os.path.join(BASE_DIR, "combio_app") + "/static/combio_app/metadata_scheme/combio_metadata_scheme.json",
    encoding="utf-8",
) as schema_file:
    metadata_schema = json.loads(schema_file.read())


def get_default_data():
    return {}


class Collection(models.Model):
    name = models.CharField(max_length=512)
    link = models.CharField(max_length=512, default="")

    def __str__(self):
        return self.name

    @property
    def nr_records(self):
        return self.record_set.count

    class Meta:
        ordering = ["name"]


class BioTag(models.Model):
    name = models.CharField(max_length=512)
    label = models.CharField(max_length=512)

    def __str__(self):
        return self.name

    records = models.ManyToManyField("Record")

    class Meta:
        ordering = ["pk"]


class Record(models.Model):
    metadata = JSONField(schema=metadata_schema)

    class Meta:
        ordering = ["pk"]

    def __str__(self):
        return self.metadata["title"]

    def title(self):
        return self.metadata["title"]

    def transcript(self):
        return self.metadata["transcript"]

    def interviewers(self):
        return [p["name"] for p in self.metadata["participants"] if p["role"] == "interviewer"]

    def interviewees(self):
        return [p["name"] for p in self.metadata["participants"] if p["role"] == "interviewee"]

    def participants(self):
        return [p["name"] for p in self.metadata["participants"] if p["role"] == "participant"]

    # def save(self, *args, **kwargs):
    #     super(Record, self).save(*args, **kwargs)

    history = HistoricalRecords()

    users = models.ManyToManyField(User)
    bio_tags = models.ManyToManyField(BioTag)
    collection = models.ForeignKey(Collection, on_delete=models.PROTECT)
