from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.contrib.auth.models import User
from elasticsearch_dsl.query import SimpleQueryString
from django.core.paginator import Paginator
from django.utils.functional import LazyObject
from pprint import pprint
from jsonview.views import JsonView

import json
from django.core.serializers.json import DjangoJSONEncoder


from django.views import View
from ..models import Record, Collection
from django.db.models import Count
from ..documents import RecordDocument

from .errors import (
    ErrorView,
    Handle400View,
    Handle403View,
    Handle404View,
    Handle500View,
    HandleErrorView,
    handle500_view,
)

__all__ = [
    "ErrorView",
    "Handle400View",
    "Handle403View",
    "Handle404View",
    "Handle500View",
    "HandleErrorView",
    "handle500_view",
]


class LandingView(TemplateView):
    template_name = "combio_app/landing.html"

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["nbar"] = "landing"
        context["collections"] = Collection.objects.all()
        context["record_count"] = Record.objects.all().count()
        return context


@method_decorator(login_required, name="dispatch")
class ProtectedView(TemplateView):
    template_name = "combio_app/protected.html"


@method_decorator(login_required, name="dispatch")
class ShowRecords(TemplateView):
    template_name = "combio_app/records.html"

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["records"] = Record.objects.all()
        context["nbar"] = "records"
        return context


# https://djangotricks.blogspot.com/2018/06/data-filtering-in-a-django-website-using-elasticsearch.html
class SearchResults(LazyObject):
    def __init__(self, search_object):
        self._wrapped = search_object

    def __len__(self):
        return self._wrapped.count()

    def __getitem__(self, index):
        search_results = self._wrapped[index]
        if isinstance(index, slice):
            search_results = list(search_results)
        return search_results


@method_decorator(login_required, name="dispatch")
class SetUserRecord(JsonView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        # u = User.objects.get(request.user.id)
        record_id = data["id"]
        r = Record.objects.get(pk=record_id)
        r.users.add(request.user)
        r.save()
        return super().get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        data = json.loads(request.body)
        record_id = data["id"]
        r = Record.objects.get(pk=record_id)
        r.users.remove(request.user)
        r.save()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["success"] = True
        return context


class ShowCollections(JsonView):
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        collections = Collection.objects.all()
        context_collections = []
        for collection in collections:
            context_collections.append(
                {
                    "pk": collection.pk,
                    "name": collection.name,
                    "link": collection.link,
                    "nr_records": collection.nr_records(),
                }
            )
        context["collections"] = context_collections
        return context


@method_decorator(login_required, name="dispatch")
class Search(JsonView):
    q = ""
    c = []
    results = []
    b_on = False
    per_page = 20
    sort = "date"

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        page = data["page"]
        per_page = data["per_page"]
        self.user_id = request.user.id
        self.b_on = data["b_on"]
        self.sort = data["sort"]
        print(self.b_on)
        s = RecordDocument.search().sort(self.sort)
        self.q = data["q"]
        self.c = data["selected_collections"]
        s = s.filter("terms", collection__pk=self.c)
        if self.b_on:
            s = s.filter("match", users=self.user_id)
        if self.q:
            if "record:" in self.q:
                s = s.filter("match", id=self.q.split(":")[1])
            else:
                # s = s.query('simple_query_string',query=self.q, fuzziness='AUTO')
                s = s.filter(
                    SimpleQueryString(
                        query=self.q,
                        fields=["title^1", "transcript^2", "interviewers^1",
                                "interviewees^3", "participants", "description"],
                        default_operator="and",
                    )
                )
                s = s.highlight("title", number_of_fragments=0)
                s = s.highlight(
                    "transcript", number_of_fragments=20, fragment_size=200)
                s = s.highlight(
                    "description", number_of_fragments=20, fragment_size=60)
                s = s.highlight("interviewers", number_of_fragments=0)
                s = s.highlight("interviewees", number_of_fragments=0)
                s = s.highlight("participants", number_of_fragments=0)
        self.total_results = SearchResults(s)
        paginator = Paginator(self.total_results, per_page)
        self.results = paginator.get_page(page)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        results_json = []
        for index, result in enumerate(self.results):
            clean_result = result.to_dict()
            del clean_result["transcript"]
            clean_result["meta"] = result.meta.to_dict()
            clean_result["index"] = index
            clean_result["bookmarked"] = self.user_id in result.users
            if "users" in clean_result:
                del clean_result["users"]
            results_json.append(clean_result)
        context["results"] = results_json
        context["results_count"] = self.results.paginator.count
        return context


@method_decorator(login_required, name="dispatch")
class ShowSearch(TemplateView):
    template_name = "combio_app/search.html"

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["nbar"] = "search"
        return context


@method_decorator(login_required, name="dispatch")
class ShowHelp(TemplateView):
    template_name = "combio_app/help.html"

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["nbar"] = "help"
        return context


@method_decorator(login_required, name="dispatch")
class ShowRecord(DetailView):
    model = Record

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


@method_decorator(login_required, name="dispatch")
class CreateRecord(TemplateView):
    template_name = "combio_app/record_create.html"

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["nbar"] = "create_record"
        context["metadata_form"] = MetadataForm()
        return context
