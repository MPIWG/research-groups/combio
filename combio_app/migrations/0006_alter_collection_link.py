# Generated by Django 4.1.7 on 2023-12-07 12:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("combio_app", "0005_alter_collection_options_collection_link"),
    ]

    operations = [
        migrations.AlterField(
            model_name="collection",
            name="link",
            field=models.CharField(default="", max_length=512),
        ),
    ]
