from django.core.management.base import BaseCommand, CommandError
import random
import pprint
import json
from .utils import create_record, purge_collections, purge_records, purge_bio_tags

pp = pprint.PrettyPrinter(width=41, compact=True)


class Command(BaseCommand):
    help = "Loads dummy data"

    def handle(self, **options):
        purge_records()
        purge_collections()
        purge_bio_tags()
        self.stdout.write(self.style.SUCCESS("Successfully purged records, collections, and bio_tags"))
