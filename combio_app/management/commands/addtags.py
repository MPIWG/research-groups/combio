from django.core.management.base import BaseCommand, CommandError
import random
import pprint
import json
from .utils import create_record, purge_collections, purge_records, purge_bio_tags
from combio_app.models import Record, BioTag
import scispacy
import spacy

pp = pprint.PrettyPrinter(width=41, compact=True)


class Command(BaseCommand):
    help = "Adds tags to Records using Scispacy's NER"

    def add_arguments(self, parser):
        parser.add_argument("model", type=str, help="pick a model from jnl, craft, bion, or bc5")
        parser.add_argument("-p", "--purge", action="store_true", help="purge existing bio_tags")

    def handle(self, *args, **kwargs):
        model = kwargs["model"]
        purge = kwargs["purge"]
        models = {}
        models["jnl"] = "en_ner_jnlpba_md"
        models["craft"] = "en_ner_craft_md"
        models["bion"] = "en_ner_bionlp13cg_md"
        models["bc5"] = "en_ner_bc5cdr_md"
        print("- loading model (this might take a while)...")
        nlp = spacy.load(models[model])

        if purge:
            print("- purging existing tags")
            purge_bio_tags()
        print(f"- processing bio_tags for {Record.objects.all().count()} records...")
        count = 0
        for record in Record.objects.all():
            doc = nlp(record.metadata["dc"]["description"] + " " + record.metadata["combio"]["transcript"])
            for ent in list(doc.ents):
                if not str(ent).startswith("’"):
                    print(str(ent))
                    print(ent.label_)
                    bio_tag, success = BioTag.objects.get_or_create(
                        name=str(ent).lower(), label=str(ent.label_).lower()
                    )
                    record.bio_tags.add(bio_tag)
                    count += 1

        self.stdout.write(self.style.SUCCESS(f"Successfully processed {count} bio_tags"))
