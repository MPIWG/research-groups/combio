from django.core.management.base import BaseCommand, CommandError
from django.forms.models import model_to_dict
from combio_app.models import Record, Collection
import random
import pprint
import json
from .utils import create_record, purge_collections, purge_records, load_records

pp = pprint.PrettyPrinter(width=41, compact=True)


class Command(BaseCommand):
    col = Collection.objects.filter(name="Cold Spring Harbor Laboratory "
                                    "Oral History Collection")[0]

    records = col.record_set.all()

    collated_records = {}
    new_records = []

    for record in records:
        interviewees = [participant for participant in record.metadata
                        ['participants'] if participant['role'] == 'interviewee']
        interviewee = interviewees[0]["name"]

        if interviewee not in collated_records:
            collated_records[interviewee] = []
            collated_records[interviewee].append(model_to_dict(record))
        else:
            collated_records[interviewee].append(model_to_dict(record))

    for key, value in collated_records.items():
        # print(f"Key: {key}")
        base_record = value[0]
        del base_record['id']
        # del base_record["metadata"]["summary"]
        new_transcript = ""
        new_transcript += value[0]["metadata"]["title"].replace(
            key+" on", "")
        new_transcript += "\n\r"
        new_transcript += value[0]["metadata"]["transcript"]
        base_record["metadata"]["transcript"] = new_transcript
        # print(base_record["metadata"]["transcript"])
        for col_record in value[1:]:
            metadata = col_record["metadata"]
            theme = ""
            theme = col_record["metadata"]["title"].replace(
                key+" on", "")
            new_transcript += "\n\r"
            new_transcript += "\n\r"
            new_transcript += theme
            new_transcript += "\n\r"
            new_transcript += col_record["metadata"]["transcript"]
            base_record["metadata"]["transcript"] += new_transcript
        new_records.append(base_record)

    # print(new_records[2]["metadata"]["transcript"])

    # collection_name = "Cold Harbor 2"
    # collection, created = Collection.objects.get_or_create(
    #    name=collection_name)
    # collection.record_set.all().delete()
    # for new_record in new_records:
    #     r = Record(metadata=new_record["metadata"])
    #     r.collection = collection
    #     r.save()

    for r in col.record_set.all():
        r.metadata["title"] = r.metadata["description"].replace(".", "")
        r.save()

    def handle(self, **options):
        self.stdout.write(self.style.SUCCESS("Successfully collated records"))
