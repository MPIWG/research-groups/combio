#!/usr/bin/env python
from combio_app.management.commands.utils import create_record
from django.core.management.base import BaseCommand, CommandError
from combio_app.models import Record, Collection, BioTag
import django
import sys
from pathlib import Path
import os
import re
import dateutil
import openai
import tiktoken
import textwrap
from time import monotonic
from langchain import PromptTemplate, LLMChain
from langchain.output_parsers import ResponseSchema, StructuredOutputParser
from langchain.llms import OpenAI
from langchain.chat_models import ChatOpenAI
from langchain.text_splitter import RecursiveCharacterTextSplitter, CharacterTextSplitter
from langchain.chains.summarize import load_summarize_chain
from langchain.docstore.document import Document

BASE_DIR = Path(__file__).resolve().parent.parent.parent


class Command(BaseCommand):
    help = "Loop through records and get summary"

    def handle(self, **options):
        def num_tokens_from_string(string: str, encoding_name: str) -> int:
            encoding = tiktoken.encoding_for_model(encoding_name)
            num_tokens = len(encoding.encode(string))
            return num_tokens

        verbose = True

        sys.path.append("/code/")
        os.environ["DJANGO_SETTINGS_MODULE"] = "project.settings"

        django.setup()

        with open("/code/env/openai.txt") as f:
            openai_key = f.read()
            os.environ["OPENAI_API_KEY"] = openai_key

        print(openai_key)

        model_name = "gpt-3.5-turbo"
        model_max_tokens = 4000  # gpt-3.5-turbo

        # model_max_tokens = 8192  # gpt4
        # model_max_tokens = 32768  # gpt-4-32k

        temperature = 0.0

        tiktoken_text_splitter = CharacterTextSplitter.from_tiktoken_encoder(
            chunk_size=2000, chunk_overlap=0)

        llm = ChatOpenAI(model_name=model_name, temperature=temperature)
        # chain = load_summarize_chain(llm, chain_type="stuff")

        COLLECTION_NAME = "National Institute of Health - History of Health Services Oral History Collection"

        # ^ setting up

        summary_schema = ResponseSchema(
            name="summary", description="a summary of the provided transcript")
        response_schemas = [summary_schema]
        output_parser = StructuredOutputParser.from_response_schemas(
            response_schemas)
        format_instructions = output_parser.get_format_instructions()
        print(format_instructions)
        template_string = """Write a concise summary of the following:
        
        
        {text}
        
        
        CONCISE SUMMARY:"""

        prompt = PromptTemplate.from_template(template_string)

        chunk_size = 3000
        overlap = 50
        print(chunk_size)
        print(overlap)

        text_splitter = RecursiveCharacterTextSplitter(
            # Set a really small chunk size, just to show.
            chunk_size=chunk_size,
            chunk_overlap=overlap,
            length_function=len,
            add_start_index=True,
        )

        collection = Collection.objects.all()[3]
        for record in Record.objects.all():
            print("RECORD ID")
            print(record.id)
            if "summary" in record.metadata and record.metadata["summary"]:
                print("ALREADY THERE")
                next
            else:
                docs = text_splitter.create_documents([record.transcript()])
                print(f"DOC LENGTH: {len(docs)}")
                num_tokens = num_tokens_from_string(
                    record.transcript(), model_name)
                print(f"NUM TOKENS: {num_tokens}")
                print(f"MODEL MAX TOKENS: {model_max_tokens}")
                prompt = PromptTemplate(template=template_string,
                                        input_variables=["text"])

                if num_tokens < model_max_tokens:
                    print("STUFF")
                    chain = load_summarize_chain(
                        llm, chain_type="stuff", prompt=prompt, verbose=verbose)
                else:
                    print("MAP_REDUCE")
                    chain = load_summarize_chain(
                        llm, chain_type="map_reduce", map_prompt=prompt, combine_prompt=prompt, verbose=verbose
                    )
                start_time = monotonic()
                summary = chain.run(docs)
                print(f"Record ID: {record.pk}")
                print(f"Chain type: {chain.__class__.__name__}")
                print(f"Run time: {monotonic() - start_time}")
                print(f"Summary: {textwrap.fill(summary, width=100)}")
                metadata = record.metadata
                metadata["summary"] = summary
                record.metadata = metadata
                record.save()
