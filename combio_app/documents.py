from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from django.contrib.auth.models import User
from .models import Record, Collection, BioTag
from dateutil.parser import parse


def is_valid_date(date):
    if date:
        try:
            parse(date)
            return True
        except:
            return False
    return False


@registry.register_document
class RecordDocument(Document):
    collection = fields.ObjectField(properties={"pk": fields.KeywordField(), "name": fields.TextField()})
    tags = fields.ListField(fields.ObjectField(properties={"name": fields.TextField(), "label": fields.TextField()}))
    metadata = fields.NestedField()
    title = fields.TextField()
    transcript = fields.TextField()
    permalink = fields.TextField()
    interviewers = fields.ListField(fields.TextField())
    interviewees = fields.ListField(fields.TextField())
    participants = fields.ListField(fields.TextField())
    description = fields.TextField()
    summary = fields.TextField()
    media_type = fields.TextField()

    # extra fields for sorting
    date = fields.DateField()
    title_kw = fields.KeywordField()

    users = fields.ListField(fields.TextField())

    def prepare_date(self, instance):
        if is_valid_date(instance.metadata["date"]):
            return instance.metadata["date"]

    def prepare_users(self, instance):
        return [u.id for u in instance.users.all()]

    def prepare_tags(self, instance):
        return [{"name": bt.name, "label": bt.label} for bt in instance.bio_tags.all()]

    def prepare_transcript(self, instance):
        if "transcript" in instance.metadata:
            return instance.metadata["transcript"]
        else:
            return ""

    def prepare_title_kw(self, instance):
        if "title" in instance.metadata:
            return instance.metadata["title"]
        else:
            return "no title"

    def prepare_title(self, instance):
        if "title" in instance.metadata:
            return instance.metadata["title"]
        else:
            return "no title"

    def prepare_description(self, instance):
        if "description" in instance.metadata:
            return instance.metadata["description"]
        else:
            return ""

    def prepare_summary(self, instance):
        if "summary" in instance.metadata:
            return instance.metadata["summary"]
        else:
            return ""
    
    def prepare_media_type(self, instance):
        if "media_type" in instance.metadata:
            return instance.metadata["media_type"]
        else:
            return ""

    def prepare_metadata(self, instance):
        return instance.metadata

    def prepare_interviewers(self, instance):
        if "participants" in instance.metadata:
            return instance.interviewers()
        else:
            return []

    def prepare_interviewees(self, instance):
        if "participants" in instance.metadata:
            return instance.interviewees()
        else:
            return []

    def prepare_participants(self, instance):
        if "participants" in instance.metadata:
            return instance.participants()
        else:
            return []

    class Index:
        # Name of the Elasticsearch index
        name = "records"
        # See Elasticsearch Indices API reference for available settings
        settings = {"number_of_shards": 1, "number_of_replicas": 0, "max_result_window": 20000}

    class Django:
        model = Record  # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = ["id"]
        related_models = [Collection]

    def get_queryset(self):
        """Not mandatory but to improve performance we can select related in one sql request"""
        return super(RecordDocument, self).get_queryset().select_related("collection")

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Record instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Collection):
            return related_instance.record_set.all()

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
